SELL Question Type
------------------

Implemented by Andreas Schwenk (TH Köln): https://www.th-koeln.de/personen/andreas.schwenk/, andreas.schwenk@th-koeln.de

Project website: https://sell.f07-its.fh-koeln.de/web/

(This Moodle plugin implementation is based on https://github.com/jamiepratt/moodle-qtype_TEMPLATE)
