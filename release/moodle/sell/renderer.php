<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * sell question renderer class.
 *
 * @package    qtype
 * @subpackage sell
 * @copyright  2021 Andreas Schwenk, TH Köln (https://www.arts-and-sciences.com)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once("lib/thirdparty/ASCIIMath2TeX.php");

defined('MOODLE_INTERNAL') || die();



/**
 * Generates the output for sell questions.
 *
 * @copyright  THEYEAR YOURNAME (YOURCONTACTINFO)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_sell_renderer extends qtype_renderer {
    public function formulation_and_controls(question_attempt $qa,
            question_display_options $options) {

        $question = $qa->get_question();
        //var_dump(get_object_vars($question));

        /* TODO:
        $questiontext = $question->format_questiontext($qa);
        $placeholder = false;
        if (preg_match('/_____+/', $questiontext, $matches)) {
            $placeholder = $matches[0];
        }
        $input = '**subq controls go in here**';

        if ($placeholder) {
            $questiontext = substr_replace($questiontext, $input,
                    strpos($questiontext, $placeholder), strlen($placeholder));
        }
        */


        $questiontext = $question->questiontext;
        // convert HTML -> plaintext
        $questiontext = str_replace("<br>", "\n", $questiontext);
        $questiontext = str_replace("&nbsp;", " ", $questiontext);
        $questiontext = str_replace("<p>", "", $questiontext);
        $questiontext = str_replace("</p>", "\n", $questiontext);


        console_log("QUESTION TEXT");
        console_log($questiontext);

        $ftmp = tmpfile();
        $ftmp_path = stream_get_meta_data($ftmp)['uri'];
        fwrite($ftmp, $questiontext);
        console_log($ftmp_path);
        
        // TODO: configure node path
        $output = shell_exec('/usr/local/bin/node type/sell/lib/sell.moodle.min.js ' . $ftmp_path);
        if($output == null)
            $text = 'FAILED!';
        else
            $text = $output;

        $text = explode(">>>>>>>>>>", $text);
        $solutions = '';
        if(count($text) >= 2) {
            $solutions = $text[2];
            $text = $text[1];
        }

        console_log($text);

        //fseek($ftmp, 0);
        //echo fread($ftmp, 1024);
        fclose($ftmp); // close+remove file


        // replace `ASCIIMATH` by \(TEX\)
        $AMT = new AMtoTeX;
        $n = strlen($text);
        $expr = '';
        $isexpr = false;
        $output = '';
        for($i=0; $i<$n; $i++) {
            if($text[$i] == '`') {
                $isexpr = !$isexpr;
                if($isexpr)
                    $expr = '';
                else {
                    $tmp = format_text("\(" . $AMT->convert($expr) . "\)");
                    $tmp = str_replace("<div", "<span", $tmp);
                    $tmp = str_replace("</div", "</span", $tmp);
                    $output = $output . $tmp;
                }
                continue;
            }
            if($isexpr) {
                $expr = $expr . $text[$i];
            } else {
                $output = $output . $text[$i];
            }
        }
        $text = $output;

        //$text = 'this is a test \( x^2 \)';

        //format_text($text);
        //console_log($text);

        //$text = '<span class="filter_mathjaxloader_equation"><div class="text_to_html">this is a test <span class="nolink">\( x^2 \)</span></div></span>';
        console_log($text);

        //$text = '<span class="filter_mathjaxloader_equation">\(x^2\)</span>';


        $result = html_writer::tag('div', $text, array('class' => 'qtext'));

        /* if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                    $question->get_validation_error(array('answer' => $currentanswer)),
                    array('class' => 'validationerror'));
        }*/
        return $result;
    }

    public function specific_feedback(question_attempt $qa) {
        // TODO.
        return '';
    }

    public function correct_response(question_attempt $qa) {
        // TODO.
        return '';
    }
}
