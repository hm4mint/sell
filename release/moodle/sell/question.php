<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * sell question definition class.
 *
 * @package    qtype
 * @subpackage sell
 * @copyright  2021 Andreas Schwenk, TH Köln (https://www.arts-and-sciences.com)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once("lib/thirdparty/log.php");

defined('MOODLE_INTERNAL') || die();


/**
 * Represents a sell question.
 *
 * @copyright  2021 Andreas Schwenk, TH Köln (https://www.arts-and-sciences.com)

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_sell_question extends question_graded_automatically_with_countback {

    public function get_expected_data() {
        // TODO.
        console_log("called get_expected_data()");
        return array();
    }

    public function summarise_response(array $response) {
        // TODO.
        console_log("called summarise_response()");
        return null;
    }

    public function is_complete_response(array $response) {
        // TODO.
        console_log("called is_complete_response()");
        return true;
    }

    public function get_validation_error(array $response) {
        // TODO.
        console_log("called get_validation_error()");
        return '';
    }

    public function is_same_response(array $prevresponse, array $newresponse) {
        // TODO.
        console_log("called is_same_response()");
        return question_utils::arrays_same_at_key_missing_is_blank(
                $prevresponse, $newresponse, 'answer');
    }


    public function get_correct_response() {
        // TODO.
        console_log("called get_correct_response()");
        return array();
    }


    public function check_file_access($qa, $options, $component, $filearea,
            $args, $forcedownload) {
        // TODO.
        console_log("called check_file_access()");
        if ($component == 'question' && $filearea == 'hint') {
            return $this->check_hint_file_access($qa, $options, $args);

        } else {
            return parent::check_file_access($qa, $options, $component, $filearea,
                    $args, $forcedownload);
        }
    }

    public function grade_response(array $response) {
        // TODO.
        console_log("called grade_response()");
        $fraction = 0;
        return array($fraction, question_state::graded_state_for_fraction($fraction));
    }

    public function compute_final_grade($responses, $totaltries) {
        // TODO.
        console_log("called compute_final_grade()");
        return 0;
    }
}
