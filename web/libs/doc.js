var doc_cols = 8;

var html = "";

function put_header(headline, subtext) {
	s = `
		<div class="jumbotron jumbotron-fluid py-3">
			<div class="container">
				<h1 class="font-weight-lighter">` + headline + `</h1>
				<p class="lead"><small>` + subtext + `</small></p>
			</div>
		</div>
	`;
	html += s;
}

function put_section(txt) {
	s = `
		<div class="row">
			<div class="col-lg-` + doc_cols + ` bg-secondary rounded shadow text-white mb-3">
				<h1 class="display-5 font-weight-light">
					` + txt + `
				</h1>
			</div>
		</div>
	`;
	html += s;
}

function put_subsection(txt) {
	s = `
		<div class="row pt-4">
			<div class="col-lg-` + doc_cols + `">
				<h3 class="font-weight-light">
					` + txt + `
				</h3>
			</div>
		</div>
	`;
	html += s;
}

function put_subsubsection(txt) {
	s = `
		<div class="row py-3">
			<div class="col-lg-` + doc_cols + `">
				<h5 class="font-weight-light">
					` + txt + `
				</h5>
			</div>
		</div>
	`;
	html += s;
}

function put_separator() {
	s = `
		</div>
		<div class="border-top my-4"></div>
		<div class="container">
	`;
	html += s;
}

function put_paragraph(p) {
	let only_ws = true;
	for(let i=0; i<p.length; i++) {
		let c = p[i];
		if(c != ' ' && c != '\t' && c != '\n') {
			only_ws = false;
			break;
		}
	}
	if(only_ws)
		return;
	//p = "xx";
	s = `
		<div class="row">
			<div class="col-lg-` + doc_cols + `">
				<p class="text-justify py-2">
					` + p + `
				</p>
			</div>
		</div>
	`;
	html += s;
}

function put_code(txt) {
	let code = txt.replace(/\</g, "&lt;").replace(/\>/g, "&gt;");
	s = `
		<div class="row pby1 px-3 my-2">
			<div class="col-lg-` + (doc_cols) + ` border rounded shadow">
				<pre><code class="text-monospace">\n` + code + `</code></pre>
			</div>
		</div>
	`;
	html += s;
}

function format(s) {
	let res = '';
	let bold = false;
	let italic = false;
	for(let i=0; i<s.length; i++) {
		let ch = s[i];
		let ch2 = '';
		let ch3 = '';
		if(i < s.length-1)
			ch2 = s[i+1];
		if(i < s.length-2)
			ch3 = s[i+2];
		if(ch=='_' && ch2=='_' && ch3=='_') {
			italic = !italic;
			res += italic ? '<i>' : '</i>';
			i += 2;
		}
		else if(ch=='_' && ch2=='_') {
			bold = !bold;
			res += bold ? '<b>' : '</b>';
			i ++;
		}
		else if(ch=='\\' && ch2=='#') {
			res += '#'
			i ++;
		}
		else if(ch=='\\' && ch2=='_') {
			res += '_'
			i ++;
		}
		else if(ch=='#') {
			let tag = '';
			i ++;
			while(i<s.length && ((s[i]>='A'&&s[i]<='Z') || (s[i]>='a'&&s[i]<='z') || s[i]=='_')) {
				tag += s[i];
				i ++;
			}
			res += '<a href="#' + tag + '">' + tag + '</a> ';
		}
		else
			res += ch;
	}

	res = res.replace(/Example:/g, "<span class=\"text-light bg-dark\"><b>&nbsp;Example:&nbsp;</b></span>");
	res = res.replace(/INPUTBOXLARGE/g, "<span class=\"border\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
	res = res.replace(/INPUTBOX/g, "<span class=\"border\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
	res = res.replace(/->/g, "&#8594;");

	return res;
}

function create_anchor(id) {
	let anchor_id = id.toLowerCase().replace(/\ /g, "_");
	 return '<span style="position:relative;"><a class="anchor" name="' + anchor_id + '"></a><span>\n';
}

function generate_doc(raw, title, subtitle) {
	let section_no = 0;
	let subsection_no = 0;
	let subsubsection_no = 0;
	let code_mode = false;
	let list_depth = 0;
	let code = '';
	let paragraph = '';

	let tocHTML = "";
	html = "";

	put_header(title, subtitle);

	html += '<div class="container">';

	let lines = raw.split("\n");
	for(let i=0; i<lines.length; i++) {
		let line = lines[i];
		if(!code_mode && line.startsWith("%") && !line.startsWith("%%%"))
			continue;
		if(line.startsWith("# ")) {
			put_paragraph(paragraph);
			paragraph = '';
			if(section_no == 0) {
				//put_separator();
				// TODO: TOC
				//document.getElementById("content").innerHTML += '<div class="bg-dark text-white"><div class="container"><div class="row"><div class="col-lg-' + doc_cols + ' px-3 py-3"><h4>Table of Contents:</h4><div id="toc"></div></div></div></div></div><br/>';
				html += `</div>
				<div class="border-top my-4"></div>
				<div class="container">`;
				put_section("Table of Contents");
				html += `<div id="toc"></div></div>
				<div class="border-top my-4"></div>
				<div class="container">`;
			} else
				put_separator();
			section_no ++;
			subsection_no = 0;
			subsubsection_no = 0;
			section_id = line.substring(2);
			html += create_anchor(section_id);
			//alert(html)
			put_section(section_no + " " + section_id);
			tocHTML += "<b>" + section_no + " " + section_id + "</b><br/>";
		}
		else if(line.startsWith("## ")) {
			//paragraph += '<hr/>';
			put_paragraph(paragraph);
			paragraph = '';
			subsection_no ++;
			subsubsection_no = 0;
			put_subsection(section_no + "." + subsection_no + " " + line.substring(3));
			tocHTML += "&nbsp;&nbsp;&nbsp;" + section_no + "." + subsection_no + " " + line.substring(3) + "<br/>";
		}
		else if(line.startsWith("### ")) {
			put_paragraph(paragraph);
			paragraph = '';
			subsubsection_no ++;
			put_subsubsection(section_no + "." + subsection_no + "." + subsubsection_no + " " + line.substring(4));
			tocHTML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>" + section_no + "." + subsection_no + "." + subsubsection_no + " " + line.substring(3) + "</small><br/>";
		}
		else if(!code_mode && line.startsWith("```")) {
			code_mode = true;
			code = '';
		}
		else if(code_mode && line.startsWith("```")) {
			put_paragraph(paragraph);
			paragraph = '';
			put_code(code);
			code_mode = false;
			code = '';
		}
		else if(line.startsWith("example...")) {
			put_paragraph(paragraph);
			paragraph = '';
			html += `<small>
					<div class="col-lg-` + doc_cols + ` border bg-light rounded shadow pt-2 mt-1 mb-4">
						<b>Example:</b>`;
		}
		else if(line.startsWith("...example")) {
			put_paragraph(paragraph);
			paragraph = '';
			html += `</div></small>`;
		}
		else {
			if(code_mode)
				code += line + '\n';
			else {
				if(line.length==0) {
					put_paragraph(paragraph);
					paragraph = '';
				}
				else {
					if(line.startsWith('* ')) {
						if(list_depth == 0)
							paragraph += '<ul>';
						else if(list_depth == 2)
							paragraph += '</ul>';
						list_depth = 1;
						paragraph += '<li>' + format(line.substring(2)) + '</li>';
					}
					else if(line.startsWith('\t*')) {
						if(list_depth == 1)
							paragraph += '<ul>';
						list_depth = 2;
						paragraph += '<li>' + format(line.substring(2)) + '</li>';
					}
					else {
						if(list_depth == 2) {
							paragraph += '</ul></ul>';
						}
						else if(list_depth == 1) {
							paragraph += '</ul>';
						}
						list_depth = 0;
						paragraph += ' ' + format(line);
					}
				}
			}

		}

	}

	put_paragraph(paragraph);
	paragraph = '';

	html += '</div>';

	document.getElementById("content").innerHTML = html;

	document.getElementById("toc").innerHTML = tocHTML;

	//alert(document.getElementById("content").innerHTML)
}

function render_page(filename, title, subtitle, cols){
	doc_cols = cols;
	let timestamp = ts = Math.round((new Date()).getTime() / 1000);
	let path = filename + '?time=' + timestamp;
	$.ajax({
		url: path,
		type: 'GET',
		success: function(data,status,xhr) {
			generate_doc(xhr.responseText, title, subtitle);
		},
		error: function(xhr, status, error) {
			alert("ERROR: " + xhr.responseText);
		}
	});
	setTimeout(function(){
		MathJax.typeset();
	},
	750);
}
