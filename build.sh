#!/usr/local/bin/python3

#******************************************************************************
#* SELL - SIMPLE E-LEARNING LANGUAGE                                          *
#*                                                                            *
#* Copyright (c) 2019-2021 TH Koeln                                           *
#* Author: Andreas Schwenk, contact@compiler-construction.com                 *
#*                                                                            *
#* Funded by: Digitale Hochschule NRW                                         *
#* https://www.dh.nrw/kooperationen/hm4mint.nrw-31                            *
#*                                                                            *
#* GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
#*                                                                            *
#* This library is licensed as described in LICENSE, which you should have    *
#* received as part of this distribution.                                     *
#*                                                                            *
#* This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
#* KIND, either impressed or implied.                                         *
#******************************************************************************/

# this script builds libs/sell.min.js

import os, sys

os.system('python3 gen-doc.py')

os.chdir("src/")

makedist = len(sys.argv) > 1 and sys.argv[1] == 'makedist'

print('HINT: create zipped dist-version via "./build.sh makedist"')

inputFiles = [ 
	"sellSymbolic.js",
	"sellLinAlg.js",
	"sell.js"
]

res = ''

def import_js(path):
	res = ''
	f = open(path, "r")
	l = f.readlines()
	f.close()
	for li in l:
		if li.startswith('/*nodejs*/'):
			continue
		if li.startswith('import ') or li.startswith('export '):
			continue
		if li.startswith('const require'):
			continue
		if li.startswith('const assert'):
			continue
		res += li
	return res

for f in inputFiles:
	res += import_js(f)

assert_impl = "function assert(exp, msg='') { if(!exp) { alert(msg); } }\n\n"

res = assert_impl + res.replace("assert(false,", "alert(")

f = open("libs/sell.js", "w")
f.write(res)
f.close()

os.system("closure-compiler -O SIMPLE_OPTIMIZATIONS --js libs/sell.js > libs/sell.min.js")

os.remove("libs/sell.js")


# make distribution
if makedist:

	os.system("cp libs/sell.min.js ../release/standalone/")
	os.system("cp libs/sell.min.js ../release/mumie/")

	os.system("cp libs/sell.min.js ../release/moodle/sell.moodle.min.js")
	
	f = open("./sell-moodle-dist.js", "r")
	moodle_code = f.read()
	f.close()
	with open("../release/moodle/sell.moodle.min.js", 'a') as f:
		f.write(moodle_code)

	copyright = '''/* %%% mumie metainfos %%%
 *
 * @name[en] Javascript for controlling sell
 * @name[de] Javascript zur Steuerung von Sell
 *
 * @description[en]
 * 	Javascript for controlling sell
 *
 * @description[de]
 *   Javascript zur Steuerung von Sell
 *
 * @copyright Copyright (C) 2021 Andreas Schwenk
 */
'''
	with open("../release/mumie/sell.min.js", 'r+') as f:
		content = f.read()
		f.seek(0, 0)
		f.write(copyright + content)

	os.system("rm ../dist.zip")
	os.system("mkdir -p ../dist/")
	os.system("cp -r index.html index-offline.html ../dist/")
	os.system("cp -r img ../dist/")
	os.system("cp -r libs ../dist/")
	os.system("cp -r training ../dist/")
	os.system("cd .. && zip -r dist.zip dist/")
	os.system("rm -r ../dist/")
