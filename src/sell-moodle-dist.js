
// PLACE CONTENT OF  sell.min.js  HERE

/*nodejs*/import math from 'mathjs';

import { createRequire } from "module";
const require = createRequire(import.meta.url);
const fs = require("fs");


let questions = '';

/*questions=`Ableitungen

    a, b in { 3, 4, ..., 8 }
    f1(x) := a * exp(b*x)
    f1_deriv(x) := diff(f1, x)

Sei $ f(x) = f1 $.
Berechne $ f'(x) = #(f1_deriv) $.
`;*/

if(process.argv.length != 3) {
    console.log('usage: node sell.min.node.js FILE_PATH_TO_QUESTION');
    process.exit(-1);
}
let qpath = process.argv[2];
try {
    questions = fs.readFileSync(qpath, 'utf8');    
} catch(err) {
    console.log("input path does not exist: " + qpath);
    process.exit(-1);
}
//console.log('question path is ' + qpath);

//console.log(questions);

let ok = true;
var sell = new Sell("en", "sell", false, "moodle");
ok = sell.importQuestions(questions);
console.log(sell.log);
if(ok) {
    console.log('>>>>>>>>>>');
    console.log(sell.html);
    console.log('>>>>>>>>>>');
    console.log(sell.variablesJsonStr);
}
else
    console.log('%%%ERROR')
